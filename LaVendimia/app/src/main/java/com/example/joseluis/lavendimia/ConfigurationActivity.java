package com.example.joseluis.lavendimia;

import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

public class ConfigurationActivity extends AppCompatActivity implements View.OnClickListener{
    EditText edtTaza;
    EditText edtEnganche;
    EditText edtPlazoMx;
    Button btnAceptar;
    Button btnCancelar;
    float sTaza;
    float sEnganche;
    float sPlazoMx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        edtTaza = (EditText) findViewById(R.id.edt_taza_financiamiento);
        edtEnganche = (EditText) findViewById(R.id.edt_enganche);
        edtPlazoMx = (EditText) findViewById(R.id.edt_plazo_maximo);
        btnAceptar = (Button) findViewById(R.id.btn_aceptar_conf);
        btnCancelar = (Button) findViewById(R.id.btn_cancelar_conf);
        btnAceptar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_aceptar_conf:
                if(checkInfo()){
                    Snackbar.make(v,"Bien Hecho. La configuración ha sido registrada",Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.btn_cancelar_conf:
                onBackPressed();
                break;
        }

    }

    public boolean checkInfo(){
       return true;
    }
}
