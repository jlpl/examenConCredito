package com.example.joseluis.lavendimia.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.joseluis.lavendimia.R;
import com.example.joseluis.lavendimia.models.Client;
import com.example.joseluis.lavendimia.models.Sale;

import java.util.List;

/**
 * Created by joseluis on 8/10/17.
 */

public class AdapterArticle extends  RecyclerView.Adapter<AdapterArticle.ArticleViewHolder>{
    public static interface AdapterArticleItemCallBacks {
        public void OnArticleItemSelected(Client client);
    }

    private AdapterArticleItemCallBacks adapterClientItemCallBacks; //Objeto de la interfaz
    private List<Sale> items; //Lista de elementos que vas a enbeber en la lista
    private Context context;

    public AdapterArticle(List<Sale> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflamos el cardview
        View cardview = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_article, parent, false);
        return new ArticleViewHolder(cardview);

    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        holder.articlename.setText(items.get(position).getSaleId());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ArticleViewHolder extends RecyclerView.ViewHolder {
        TextView articlename;
        ArticleViewHolder(View itemView) {
            super(itemView);
            articlename = (TextView) itemView.findViewById(R.id.edt_articletname_cardview);
        }
    }
}
