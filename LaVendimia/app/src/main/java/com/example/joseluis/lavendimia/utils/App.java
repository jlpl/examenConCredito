package com.example.joseluis.lavendimia.utils;

import android.app.Application;

import com.squareup.otto.Bus;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by joseluis on 8/10/17.
 */

public class App extends Application{
    private static Bus bus;
    private static Realm realm;
    @Override
    public void onCreate() {
        super.onCreate();
        bus = new Bus();

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();
    }

    public static Bus getBus() {
        return bus;
    }
    public static Realm getRealm(){
        return realm;
    }

}
