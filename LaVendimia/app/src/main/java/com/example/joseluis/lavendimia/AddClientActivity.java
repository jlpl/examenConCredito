package com.example.joseluis.lavendimia;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.joseluis.lavendimia.models.Client;
import com.example.joseluis.lavendimia.utils.App;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class AddClientActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText edtName;
    private EditText edtFatherLastName;
    private EditText edtMotherLastName;
    private EditText edtRFC;
    private TextView clave;
    private Button btnAccept;
    private Button btnCancel;
    private Realm realm;
    private int clv = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_client);
        edtName = (EditText) findViewById(R.id.edt_clientname);
        edtFatherLastName = (EditText) findViewById(R.id.edt_clientfatherlastname);
        edtMotherLastName = (EditText) findViewById(R.id.edt_clientmotherlastname);
        edtRFC = (EditText) findViewById(R.id.edt_clientRFC);
        clave = (TextView) findViewById(R.id.txv_clavecliente);
        btnAccept = (Button) findViewById(R.id.btn_aceptar_addclient);
        btnCancel = (Button) findViewById(R.id.btn_cancelar_addclient);
        realm = App.getRealm();
        btnAccept.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        clv = getNextClientId();
        }


    @Override
    protected void onResume() {
        super.onResume();
        clave.setText(String.valueOf(clv));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_aceptar_addclient:
                String name = edtName.getText().toString();
                String fatherLN = edtFatherLastName.getText().toString();
                String motherLN = edtMotherLastName.getText().toString();
                String RFC = edtRFC.getText().toString();
                addClient(name,fatherLN,motherLN,RFC);
                Snackbar.make(v,"Bien Hecho. El cliente ha sido registrado correctamente”",Snackbar
                        .LENGTH_LONG).show();
                onBackPressed();
                break;
            case R.id.btn_cancelar_addclient:
                onBackPressed();
                break;
        }
    }
    private void addClient(String name, String fatherLN, String motherLN,String RFC){
        realm.beginTransaction();
        Client client = new Client();
        client.setClientName(name + " " + fatherLN + " " + motherLN);
        client.setRfc(RFC);
        client.setClientID(clv);
        realm.copyToRealm(client);
        realm.commitTransaction();
    }
    public int getNextClientId() {
        try {
            Number number = realm.where(Client.class).max("clientID");
            if (number != null) {
                return number.intValue() + 1;
            } else {
                return 0;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }
}
