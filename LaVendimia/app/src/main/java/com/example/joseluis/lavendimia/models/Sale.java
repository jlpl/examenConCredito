package com.example.joseluis.lavendimia.models;

/**
 * Created by joseluis on 8/9/17.
 */

public class Sale {
    private int saleId;
    private int saleClientId;
    private String saleClientName;
    private int saleTotal;
    private String saleDate;

    public Sale() {
        this.saleId = 0;
        this.saleClientId = 0;
        this.saleClientName = "";
        this.saleTotal = 0;
        this.saleDate = "";
    }

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public int getSaleClientId() {
        return saleClientId;
    }

    public void setSaleClientId(int saleClientId) {
        this.saleClientId = saleClientId;
    }

    public String getSaleClientName() {
        return saleClientName;
    }

    public void setSaleClientName(String saleClientName) {
        this.saleClientName = saleClientName;
    }

    public int getSaleTotal() {
        return saleTotal;
    }

    public void setSaleTotal(int saleTotal) {
        this.saleTotal = saleTotal;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }
}
