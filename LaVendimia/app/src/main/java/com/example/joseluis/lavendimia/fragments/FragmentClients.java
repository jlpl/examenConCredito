package com.example.joseluis.lavendimia.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.joseluis.lavendimia.AddClientActivity;
import com.example.joseluis.lavendimia.R;
import com.example.joseluis.lavendimia.adapters.AdapterClient;
import com.example.joseluis.lavendimia.models.Client;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joseluis on 8/10/17.
 */

public class FragmentClients extends Fragment {
    private FloatingActionButton fb;
    private RecyclerView mRecyclerView;
    private AdapterClient adapterClient;
    private List<Client> mList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_clients,container,false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.rcv_clientes);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        Client c = new Client();
        c.setClientName("jose");
        mList.add(c);
        adapterClient = new AdapterClient(mList,getContext());
        return v;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView.setAdapter(adapterClient);
        fb = (FloatingActionButton) getActivity().findViewById(R.id.fab_clients);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddClientActivity.class);
                startActivity(intent);
                Snackbar.make(getView(),"Agregar Clientes",Snackbar.LENGTH_LONG).show();
            }
        });
    }
}
