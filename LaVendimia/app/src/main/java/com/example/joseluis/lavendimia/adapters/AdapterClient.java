package com.example.joseluis.lavendimia.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.joseluis.lavendimia.R;
import com.example.joseluis.lavendimia.models.Client;

import java.util.List;

/**
 * Created by joseluis on 8/10/17.
 */

public class AdapterClient extends  RecyclerView.Adapter<AdapterClient.ClientViewHolder>{
    public static interface AdapterClientItemCallBacks {
        public void OnClientItemSelected(Client client);
    }

    private AdapterClientItemCallBacks adapterClientItemCallBacks; //Objeto de la interfaz
    private List<Client> items; //Lista de elementos que vas a enbeber en la lista
    private Context context;

    public AdapterClient(List<Client> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ClientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflamos el cardview
        View cardview = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_client, parent, false);
        return new ClientViewHolder(cardview);

    }

    @Override
    public void onBindViewHolder(ClientViewHolder holder, int position) {
        holder.name.setText(items.get(position).getClientName());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ClientViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ClientViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.edt_clientname_cardview);
        }
    }
}
