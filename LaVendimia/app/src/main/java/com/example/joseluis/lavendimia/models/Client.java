package com.example.joseluis.lavendimia.models;

import io.realm.RealmObject;

/**
 * Created by joseluis on 8/9/17.
 */

public class Client extends RealmObject{
    private String clientName;
    private String rfc;
    private int clientID;

    public Client() {
        this.clientName = "";
        this.clientID = 0;
        this.rfc = "";
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }
}
