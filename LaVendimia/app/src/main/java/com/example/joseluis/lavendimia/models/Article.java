package com.example.joseluis.lavendimia.models;

/**
 * Created by joseluis on 8/9/17.
 */

public class Article {
    private String articleName;
    private String articleModel;
    private String articleDescription;
    private int articleExistence;
    private int articlePrice;

    public Article( ) {
        this.articleName = "";
        this.articleModel = "";
        this.articleDescription = "";
        this.articleExistence = 0;
        this.articlePrice = 0;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getArticleModel() {
        return articleModel;
    }

    public void setArticleModel(String articleModel) {
        this.articleModel = articleModel;
    }

    public String getArticleDescription() {
        return articleDescription;
    }

    public void setArticleDescription(String articleDescription) {
        this.articleDescription = articleDescription;
    }

    public int getArticleExistence() {
        return articleExistence;
    }

    public void setArticleExistence(int articleExistence) {
        this.articleExistence = articleExistence;
    }

    public int getArticlePrice() {
        return articlePrice;
    }

    public void setArticlePrice(int articlePrice) {
        this.articlePrice = articlePrice;
    }
}
