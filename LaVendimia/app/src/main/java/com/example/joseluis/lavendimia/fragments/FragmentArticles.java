package com.example.joseluis.lavendimia.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.joseluis.lavendimia.AddClientActivity;
import com.example.joseluis.lavendimia.R;

/**
 * Created by joseluis on 8/10/17.
 */

public class FragmentArticles extends Fragment {
    FloatingActionButton fb;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_articles,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       fb = (FloatingActionButton) getActivity().findViewById(R.id.fab_articles);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Snackbar.make(getView(),"Agregar Articulo",Snackbar.LENGTH_LONG).show();
            }
        });
    }
}
