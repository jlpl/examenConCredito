package com.example.joseluis.lavendimia.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.joseluis.lavendimia.R;
import com.example.joseluis.lavendimia.models.Client;
import com.example.joseluis.lavendimia.models.Sale;

import java.util.List;

/**
 * Created by joseluis on 8/10/17.
 */

public class AdapterSale extends  RecyclerView.Adapter<AdapterSale.SaleViewHolder>{
    public static interface AdapterSaleItemCallBacks {
        public void OnSaleItemSelected(Client client);
    }

    private AdapterSaleItemCallBacks adapterClientItemCallBacks; //Objeto de la interfaz
    private List<Sale> items; //Lista de elementos que vas a enbeber en la lista
    private Context context;

    public AdapterSale(List<Sale> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public SaleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflamos el cardview
        View cardview = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_sale, parent, false);
        return new SaleViewHolder(cardview);

    }

    @Override
    public void onBindViewHolder(SaleViewHolder holder, int position) {
        holder.saleid.setText(items.get(position).getSaleId());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class SaleViewHolder extends RecyclerView.ViewHolder {
        TextView saleid;
        SaleViewHolder(View itemView) {
            super(itemView);
            saleid = (TextView) itemView.findViewById(R.id.edt_saleid_cardview);
        }
    }
}
